<?php
/**
 * Template used to display post content on single pages.
 *
 * @package lsx-health-plan
 */

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		include LSX_HEALTH_PLAN_PATH . '/templates/content-plan.php';
	endwhile;
endif;
?>

<div class="row tab-content-plan">
	<?php
		if ( loseit_is_first_day() ) {
			loseit_weekly_must_read();
		}
	?>
</div>
