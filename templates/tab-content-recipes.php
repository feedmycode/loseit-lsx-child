<?php
/**
 * Template used to display post content on single pages.
 *
 * @package lsx-health-plan
 */
global $connected_recipe;

if ( ! empty( $connected_recipe ) && null !== $connected_recipe ) {
	$args     = array(
		'orderby'   => 'date',
		'order'     => 'DESC',
		'post_type' => 'recipe',
		'post__in'  => $connected_recipe,
	);
	$recipes = new WP_Query( $args );
	if ( $recipes->have_posts() ) {
		while ( $recipes->have_posts() ) {
			$recipes->the_post();
			include get_stylesheet_directory() . '/templates/tab-content-recipe.php';
		}
		wp_reset_postdata();
	}
}
