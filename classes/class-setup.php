<?php
/**
 * Class Setup
 *
 * @package   loseit\classes
 * @author    LightSpeed
 * @license   GPL-2.0+
 * @link
 * @copyright 2019 LightSpeed
 */

namespace loseit\classes;

/**
 * Class Setup
 *
 * @package loseit\classes
 */
class Setup {

	/**
	 * Holds class instance
	 *
	 * @since 1.0.0
	 *
	 * @var      object loseit\classes\Setup()
	 */
	protected static $instance = null;

	/**
	 * @var object \loseit\classes\Endpoints();
	 */
	public $endpoints;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function __construct() {
		add_action( 'after_setup_theme', array( $this, 'language_setup' ), 11 );
		add_action( 'after_setup_theme', array( $this, 'loseit_setup' ), 10 );

		require_once get_stylesheet_directory() . '/classes/class-endpoints.php';
		$this->endpoints = Endpoints::get_instance();
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @return    object \loseit\classes\Setup()    A single instance of this class.
	 */
	public static function get_instance() {
		// If the single instance hasn't been set, set it now.
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Load the text domain
	 *
	 * @return void
	 */
	public function language_setup() {
		load_child_theme_textdomain( 'loseit-lsx-child', get_stylesheet_directory() . '/languages' );
	}

	/**
	 * To set the new thumbnail for the Must Read Tab.
	 *
	 * @return void
	 */
	public function loseit_setup() {
		add_image_size( 'loseit-thumbnail', 265, 350, true );
	}
}
