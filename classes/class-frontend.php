<?php
/**
 * Class FrontEnd
 *
 * @package   loseit\classes
 * @author    LightSpeed
 * @license   GPL-2.0+
 * @link
 * @copyright 2019 LightSpeed
 */

namespace loseit\classes;

/**
 * Class Frontend
 *
 * @package loseit\classes
 */
class Frontend {

	/**
	 * Holds class instance
	 *
	 * @since 1.0.0
	 *
	 * @var      object loseit\classes\Frontend()
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function __construct() {
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts' ), 11 );
		add_action( 'wp_head', array( $this, 'wp_head' ), 11 );
		add_action( 'lsx_footer_before', array( $this, 'loseit_archive_recipe_bottom' ) );
		add_action( 'template_redirect', array( $this, 'loseit_woo_custom_redirect_after_purchase' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @return    object \loseit\classes\Frontend()    A single instance of this class.
	 */
	public static function get_instance() {
		// If the single instance hasn't been set, set it now.
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Enqueues the parent and the child theme styles.
	 *
	 * @package    loseit-lsx-child
	 * @subpackage setup
	 */
	public function scripts() {
		if ( defined( 'SCRIPT_DEBUG' ) ) {
			$prefix = '';
			$suffix = '';
		} else {
			$prefix = '';
			$suffix = '.min';
		}
		//https://github.com/lightspeeddevelopment/loseit-lsx-child.git
		// Fonts from LSX Theme. Add these lines if your website will use a different font.
		wp_dequeue_style( 'lsx-header-font' );
		wp_dequeue_style( 'lsx-body-font' );
		wp_dequeue_style( 'lsx_font_scheme' );

		// Google Fonts. Add these lines if your website will use a different font.
		//wp_enqueue_style( 'loseit-lsx-child-quattrocento-sans', 'https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400i,700,700i' );

		wp_enqueue_script( 'loseit-lsx-child-scripts', get_stylesheet_directory_uri() . '/assets/js/' . $prefix . 'custom' . $suffix . '.js', array( 'jquery' ) );
	}

	/**
	 * Checks to see if we should include our filters.
	 *
	 * @package    loseit-lsx-child
	 * @subpackage setup
	 */
	public function wp_head() {
		if ( is_singular( 'plan' ) ) {
			// Single Plan Filters.
			add_filter( 'lsx_health_plan_single_nav_path', array( $this, 'plan_single_nav_path' ) );
			add_filter( 'lsx_health_plan_single_tab_path', array( $this, 'plan_single_tab_path' ) );
		}
	}

	/**
	 * Outputs the Single Plan Nav
	 *
	 * @param string $path
	 * @return string
	 */
	public function plan_single_nav_path( $path ) {
		$path = get_stylesheet_directory() . '/templates/single-plan-tabs.php';
		return $path;
	}

	/**
	 * Outputs the Single Plan Tab based on the endpoint
	 *
	 * @param string $path
	 * @return string
	 */
	public function plan_single_tab_path( $path ) {
		$endpoint = get_query_var( 'endpoint' );
		if ( '' === $endpoint && ! loseit_is_first_day() ) {
			$endpoint = 'breakfast';
		}
		switch ( $endpoint ) {
			case 'breakfast':
			case 'lunch':
			case 'dinner':
				$path = get_stylesheet_directory() . '/templates/tab-content-recipes.php';
				break;

			default:
				$path = get_stylesheet_directory() . '/templates/tab-content-plan.php';
				break;
		}
		return $path;
	}

	/**
	 * Bottom CTA for recipes archive
	 *
	 * @return void
	 */
	public function loseit_archive_recipe_bottom() {
		if ( is_archive() && ( is_post_type_archive( 'recipe' ) || is_tax( 'recipe-type' ) ) ) {
			?>
			<section id="recipes-cta" class="lsx-full-width">
				<div class="row">
					<div class="col-md-4">
						<?php require get_stylesheet_directory() . '/assets/images/red-dots.svg'; ?>
					</div>
					<div class="col-md-8">
						<h3><?php echo esc_html_e( '6 Weeks to LOSE IT’s specially designed online programme won’t only help you shed kilos, it will also help you change the way you think about food.', 'loseit-lsx-child' ); ?></h3>
						<h4><?php echo esc_html_e( 'And yes, that means a shift in what you eat and how you shop.', 'loseit-lsx-child' ); ?></h4>
					</div>
				</div>
			</section>
			<?php
		}
	}

	/**
	 * Change path of thank you page (Order Received)
	 *
	 * @return void
	 */
	public function loseit_woo_custom_redirect_after_purchase() {
		global $wp;
		if ( is_checkout() && ! empty( $wp->query_vars['order-received'] ) ) {
			wp_safe_redirect( '/thank-you-eft/' );
			exit;
		}
	}
}
