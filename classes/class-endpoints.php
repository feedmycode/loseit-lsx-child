<?php
namespace loseit\classes;

/**
 * Contains the endpoints
 *
 * @package loseit
 */
class Endpoints {

	/**
	 * Holds class instance
	 *
	 * @since 1.0.0
	 *
	 * @var      object \loseit\classes\Endpoints()
	 */
	protected static $instance = null;

	/**
	 * Contructor
	 */
	public function __construct() {
		add_action( 'init', array( $this, 'setup' ), 11 );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @return    object \loseit\classes\Endpoints()    A single instance of this class.
	 */
	public static function get_instance() {
		// If the single instance hasn't been set, set it now.
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Runs on init
	 */
	public function setup() {
		$this->add_rewrite_rules();
	}

	/**
	 * Registers the rewrites.
	 */
	public function add_rewrite_rules() {
		// Here is where we add in the rewrite rules above the normal WP ones.
		add_rewrite_rule( 'plan/([^/]+)/breakfast/?$', 'index.php?plan=$matches[1]&endpoint=recipes&endpoint=breakfast', 'top' );
		add_rewrite_rule( 'plan/([^/]+)/lunch/?$', 'index.php?plan=$matches[1]&endpoint=recipes&endpoint=lunch', 'top' );
		add_rewrite_rule( 'plan/([^/]+)/dinner/?$', 'index.php?plan=$matches[1]&endpoint=recipes&endpoint=dinner', 'top' );
	}
}
