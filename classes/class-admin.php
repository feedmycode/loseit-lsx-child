<?php
/**
 * Class Admin
 *
 * @package   loseit\classes
 * @author    LightSpeed
 * @license   GPL-2.0+
 * @link
 * @copyright 2019 LightSpeed
 */

namespace loseit\classes;

/**
 * Class Admin
 *
 * @package loseit\classes
 */
class Admin {

	/**
	 * Holds class instance
	 *
	 * @since 1.0.0
	 *
	 * @var      object loseit\classes\Admin()
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function __construct() {
		add_action( 'create_week', array( $this, 'loseit_save_meta' ), 10, 2 );
		add_action( 'edited_week', array( $this, 'loseit_save_meta' ), 10, 2 );
		add_action( 'week_edit_form_fields', array( $this, 'loseit_taxonomy_edit_meta_field' ), 10, 2 );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @return    object \loseit\classes\Admin()    A single instance of this class.
	 */
	public static function get_instance() {
		// If the single instance hasn't been set, set it now.
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Adding custom image for Week descriptions.
	 *
	 * @param [type] $term
	 * @return void
	 */
	public function loseit_taxonomy_edit_meta_field( $term ) {

		if ( is_object( $term ) ) {
			$loseit_thumbnail_value = get_term_meta( $term->term_id, 'loseit_thumbnail', true );
			$image_preview_loseit_thumbnail = wp_get_attachment_image_src( $loseit_thumbnail_value, 'thumbnail' );
			if ( is_array( $image_preview_loseit_thumbnail ) ) {
				$image_preview_loseit_thumbnail = '<img src="' . $image_preview_loseit_thumbnail[0] . '" width="' . $image_preview_loseit_thumbnail[1] . '" height="' . $image_preview_loseit_thumbnail[2] . '" class="alignnone size-thumbnail wp-image-' . $loseit_thumbnail_value . '" />';
			}
		} else {
			$image_preview_loseit_thumbnail = false;
			$loseit_thumbnail_value = false;
		}
		?>
		<tr class="form-field term-thumbnail-wrap">
			<th scope="row"><label for="loseit_thumbnail"><?php esc_html_e( 'Week Featured Image', 'loseit-lsx-child' ); ?></label></th>
			<td>
				<input class="input_image_id" type="hidden" name="loseit_thumbnail" value="<?php echo esc_attr( $loseit_thumbnail_value ); ?>">
				<div class="thumbnail-preview">
					<?php echo wp_kses_post( $image_preview_loseit_thumbnail ); ?>
				</div>
				<a style="<?php if ( '' !== $loseit_thumbnail_value ) { ?>display:none;<?php } ?>" class="button-secondary lsx-thumbnail-image-add"><?php esc_html_e( 'Choose Image', 'loseit-lsx-child' ); ?></a>
				<a style="<?php if ( '' === $loseit_thumbnail_value || false === $loseit_thumbnail_value ) { ?>display:none;<?php } ?>" class="button-secondary lsx-thumbnail-image-remove"><?php esc_html_e( 'Remove Image', 'loseit-lsx-child' ); ?></a>
				<?php wp_nonce_field( 'lsx_to_save_term_loseit_thumbnail', 'lsx_to_term_loseit_thumbnail_nonce' ); ?>
			</td>
		</tr>
		<?php
	}

	/**
	 * Saves the Taxonomy term banner image
	 *
	 * @since 0.1.0
	 *
	 * @param  int    $term_id
	 * @param  string $taxonomy
	 */
	public function loseit_save_meta( $term_id = 0, $taxonomy = '' ) {

		if ( ! isset( $_POST['loseit_thumbnail'] ) ) {
			return;
		}

		if ( check_admin_referer( 'lsx_to_save_term_loseit_thumbnail', 'lsx_to_term_loseit_thumbnail_nonce' ) ) {

			$thumbnail_meta = sanitize_text_field( $_POST['loseit_thumbnail'] );
			$thumbnail_meta = ! empty( $thumbnail_meta ) ? $thumbnail_meta : '';

			if ( empty( $thumbnail_meta ) ) {
				delete_term_meta( $term_id, 'loseit_thumbnail' );
			} else {
				update_term_meta( $term_id, 'loseit_thumbnail', $thumbnail_meta );
			}
		}
	}

}
